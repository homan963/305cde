var restify = require('restify');
var server = restify.createServer();

server.use(restify.plugins.bodyParser());
server.use(restify.plugins.authorizationParser());

server.listen(8080, function(){
   console.log('incoming request being handled');
   
   server.get('/', function(req, res, next){
      console.log('root URL');
      res.header('Location', '/items.json');
      res.send(302);
      res.end();
   });
   
   server.get(/^\/items.(json|xml)$/, function(req, res, next){
       if (req.headers['if-modified-since']){ 
          var last = new Date(req.headers['if-modified-since']);
          var now = new Date();
          if (last < now) {
             console.log('update avaliable');
             res.header('Last-Modified',now);
             res.status(201);
             res.send({data: 'new data'});
          } else{
             console.log('no update');
             res.status(304);
          }
       }
       res.end();
   });
   
   server.get(/^\items\/([0-9]+).(json|xml)$/, function(req, res, next){
       console.log('matched!');
       res.end();
   });
   
   server.post(/^\/items.(json|xml)/, function (req, res, next){
      const errors = require('restify-errors');
      
      if(req.headers['context-type'] != 'application/json'){
         return next(new errors.BadRequestError("body not json formatted"));
      }
      var data = req.bod;
      req.status(201);
      req.setHeader('Location', 'items/2.json');
      req.setHeader('Content-Type', 'application/json');
      req.send(data);
      req.end();
   });
   
});