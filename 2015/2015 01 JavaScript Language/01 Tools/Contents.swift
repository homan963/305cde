//: Playground - noun: a place where people can play

import UIKit

var Grades = [18,35,65]
var average = 0
print("List Has \(Grades.count) items")
Grades.append(88)
for item in Grades{
    print ("Item in list \(item)")
    switch item{
        case 0..<40:
            print ("Sorry, you failed")
        case 70..<101:
            print ("Congratulations, a 1st")
        case 60..<70:
            print ("Not bad, a 2:1")
        case 40..<50:
            print ("That Sucks, a 3rd")
        case 50..<60:
            print ("OK, a 2:2")
        default: //Catch all
            print ("Grade outside of boundries")
        }
    average += item
}
print ("Average grade is \(average/Grades.count)")


let pi = 3.14

var circles = [1.0,2.0,5.0,10.0]

func area(pi:Double, radius: Double) -> Double {
    return pi*(radius*radius)
}

func circumference (pi: Double, radius: Double) -> Double {
    return(2*pi*radius)
}

for item in circles{
    print("Area: \(area(pi:pi,radius:item))")
    print("Circumference: \(circumference(pi:pi,radius:item))")
}

class Square{
    var height : Double
    
    init( height: Double){
        self.height = height
    }
    
    func area() -> Double {
        return (height*height)
    }
    
    func circumference() -> Double {
        return(4*height)
    }
}

class Rectangle{
    var height : Double
    var width : Double
    
    init(height: Double, width : Double){
        self.height = height
        self.width = width
    }
    
    func area() -> Double {
        return (height*width)
    }
    
    func circumference () -> Double {
        return(4*(height+width))
    }
}
var rectangle = Rectangle(height:2,width:3)
print("Rectangle Area: \(rectangle.area())")
print("Rectangle Circumference: \(rectangle.circumference())")

class Triangle{
    var height : Double
    var width : Double
    var thirdEdge : Double
    
    init(height: Double, width : Double,thirdEdge: Double){
        self.height = height
        self.width = width
        self.thirdEdge = thirdEdge
    }
    
    func area() -> Double {
        return (height*width)/2
    }
    
    func circumference () -> Double {
        return(height+width+thirdEdge)
    }
}
