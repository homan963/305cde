
var request = new XMLHttpRequest();
request.open('GET', 'data/books.json', false);
request.send(null);
var data = JSON.parse(request.responseText);
console.log(data);

var books = data.books;

var table = document.createElement('TABLE');
table.setAttribute("border","1")
table.setAttribute("id", "myTable");
var head = document.createElement('TH');
var head2 = document.createElement('TH');
head.innerHTML = "Book Title";
table.appendChild(head);
head2.innerHTML = "Production Year";
table.appendChild(head2);
for (var i=0; i < books.length; i++) {
	console.log(books[i].title);
	var row = document.createElement('TR');
	var cell = document.createElement('TD');
	var cell2 = document.createElement('TD');	
	cell.innerHTML = books[i].title;
	row.appendChild(cell);
	cell2.innerHTML = books[i].year;
	row.appendChild(cell2);
	row.setAttribute("onclick","rowOnclick('"+books[i].title+"')");
	table.appendChild(row);
	
}
function rowOnclick(title){
	console.log("1");
    document.title = title;
}
document.body.appendChild(table);
