var accounts = []; //local storage to store the accounts
var data; //store the json from firebase
var error = false;
var url = "https://modernweb1-b4e5e.firebaseio.com";
//firebase initialize
var config = {
    apiKey: "AIzaSyAg7PrPSk_ifKsgjDrJG0BcukcLG7GDz0I",
    authDomain: "modernweb1-b4e5e.firebaseapp.com",
    databaseURL: "https://modernweb1-b4e5e.firebaseio.com",
    projectId: "modernweb1-b4e5e",
    storageBucket: "modernweb1-b4e5e.appspot.com",
    messagingSenderId: "854408754696"
 };
 firebase.initializeApp(config);
var database = firebase.database();

//create account and get data from html form
document.getElementById('createForm').onsubmit=function() {
	console.log('add account');
	var username = document.querySelector('#addPage #username').value;
	var name = document.querySelector('#addPage #name').value;
	var email = document.querySelector('#addPage #email').value;
	var password = document.querySelector('#addPage #password').value;
	var age = document.querySelector('#addPage #age').value;
	var d = new Date();
	var n = ""+d.getDate()+d.getMonth()+d.getFullYear()+d.getMinutes()+d.getSeconds();
	var key = n;
	createAccount(username,name,email,password,age,key);
	accounts.push({username: username, name: name, email: email, password: password, age: age,key:key});
	loadList();
	console.log(accounts)
};

document.querySelector('#editPage #average').onclick = function() {
	console.log('Average Age');
	average();
};

document.querySelector('#editPage #name').onclick = function() {
	console.log('Sort by name');
	sortByName();
};

document.querySelector('#editPage #age').onclick = function() {
	console.log('Sort by age');
	sortByAge();
};

//search the data of age less than 
document.querySelector('#editPage #less').onclick = function() {
	var temp = document.getElementById("numAge").value;
	var table = document.getElementById('list');
	console.log(temp);
	table.innerHTML = '';
	table.innerHTML += '<tr><th>Username</th><th>Name</th><th>Email</th><th>Age</th></tr>';
	for (var userid in accounts){
		if (parseInt(accounts[userid].age)<=temp){
			var row = document.createElement('tr');
	 		row.innerHTML = '<td>'+accounts[userid].username+'</td><td>'+accounts[userid].name+'</td><td>'+accounts[userid].email+'</td><td>'+accounts[userid].age+'</td>';
			table.appendChild(row);
		}
	}
};

//search the data of age greater than 
document.querySelector('#editPage #greater').onclick = function() {
	var temp = document.getElementById("numAge").value;
	var table = document.getElementById('list');
	console.log(temp);
	table.innerHTML = '';
	table.innerHTML += '<tr><th>Username</th><th>Name</th><th>Email</th><th>Age</th></tr>';
	for (var userid in accounts){
		if (parseInt(accounts[userid].age)>temp){
			var row = document.createElement('tr');
	 		row.innerHTML = '<td>'+accounts[userid].username+'</td><td>'+accounts[userid].name+'</td><td>'+accounts[userid].email+'</td><td>'+accounts[userid].age+'</td>';
			table.appendChild(row);
		}
	}
};

function GetSortOrder(prop) {
	//compare the data
    return function(a, b) {  
        if (a[prop] > b[prop]) {  
            return 1;  
        } else if (a[prop] < b[prop]) {  
            return -1;  
        }  
        return 0;  
    }  
}  

function sortByName(){
	//sort the table by name
	var table = document.getElementById('list');
	table.innerHTML = '';
	table.innerHTML += '<tr><th>Username</th><th>Name</th><th>Email</th><th>Age</th></tr>';
	accounts.sort(GetSortOrder("name"));
	for(var userid in accounts){
		var row = document.createElement('tr');
	 	row.innerHTML = '<td>'+accounts[userid].username+'</td><td>'+accounts[userid].name+'</td><td>'+accounts[userid].email+'</td><td>'+accounts[userid].age+'</td>';
		table.appendChild(row);
	}
}

function sortByAge(){
	//sort the table by age
	var table = document.getElementById('list');
	table.innerHTML = '';
	table.innerHTML += '<tr><th>Username</th><th>Name</th><th>Email</th><th>Age</th></tr>';
	accounts.sort(GetSortOrder("age"));
	for(var userid in accounts){
		var row = document.createElement('tr');
	 	row.innerHTML = '<td>'+accounts[userid].username+'</td><td>'+accounts[userid].name+'</td><td>'+accounts[userid].email+'</td><td>'+accounts[userid].age+'</td>';
		table.appendChild(row);
	}
}

function average() {
	//calculate the average age of each accounts
	var result = 0;
	var count = 0;
	for (var userid in data){
		result += parseInt(data[userid].age);
		count++;
	}
	result = result/count;
	alert("Average age is "+result);
	console.log(result);
}


function createAccount(username,name,email,password,age,key){
	//create account to firebase
	var temp = JSON.stringify(accounts);
	for(var userid in data){
		if(username==data[userid].username)
			error = true;
	}
	if(error){
		alert("Duplicate username detected!! Please input another username.");
	} else{
	firebase.database().ref('users/' + key).set({
		username:username,
		name:name,
		email:email,
		password:password,
		age:age
	  });
	  error=false;
	}
}

function listAccount(){ 
	//onload function for getting data from firebase
	 var table = document.getElementById('list');
	 table.innerHTML = '';
	 table.innerHTML += '<tr><th>Username</th><th>Name</th><th>Email</th><th>Age</th></tr>';
	 firebase.database().ref().child('users').once('value').then(function(snapshot) {
	 	data = snapshot.val();
	 	for (var userid in data){
	 		accounts.push(data[userid]);
	 		var row = document.createElement('tr');
	 		row.innerHTML = '<td>'+data[userid].username+'</td><td>'+data[userid].name+'</td><td>'+data[userid].email+'</td><td>'+data[userid].age+'</td>';
			table.appendChild(row);
	 	}
	});
}


